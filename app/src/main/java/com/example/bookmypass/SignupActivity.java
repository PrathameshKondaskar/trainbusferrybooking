package com.example.bookmypass;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.bookmypass.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SignupActivity extends AppCompatActivity {

    //View
    private TextInputEditText etName,etEmail,etPassword,etMobile,etAddress,etDOB;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button btnSignup;
    private TextView tvLogin;
    private ProgressDialog progressDialog;

    //Variables
    String name,email,password,mobile,address,dob,gender;
    UserSharedPreference userSharedPreference;
    UserModel userModel;
    Calendar myCalendar = Calendar.getInstance();
    int mDay,mMonth,mYear;
    String  month ,day;

    //Firebase Variables;
    private FirebaseAuth mAuth;
    FirebaseFirestore mFireStore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        init();


    }

    private void init() {
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etMobile = findViewById(R.id.etMobile);
        etDOB = findViewById(R.id.etDOB);
        etAddress = findViewById(R.id.etAddress);
        radioGroup =findViewById(R.id.radioGroup);
        btnSignup = findViewById(R.id.btnSignup);
        tvLogin = findViewById(R.id.tvLogin);
        progressDialog=new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        userSharedPreference = new UserSharedPreference(this);




        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };

        etDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(SignupActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    if(monthOfYear < 10){

                                     month = "0" + (monthOfYear+1);
                                    }else {
                                        month= (monthOfYear+1)+"";
                                    }

                                    if(dayOfMonth < 10){

                                        day  = "0" + dayOfMonth ;
                                    }
                                    else {
                                        day = dayOfMonth+"";
                                    }

                                    etDOB.setText(day + "/" + month+ "/" + year);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }

                return false;
            }
        });



        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SignupActivity.this,LoginActivity.class));
                finish();
            }
        });
    }
    private void updateLabel() {

        String myFormat = "dd/mm/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDOB.setText(sdf.format(myCalendar.getTime()));
    }

    public void registerUser() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();
        name = etName.getText().toString();
         mobile = etMobile.getText().toString();
         address = etAddress.getText().toString();
         dob = etDOB.getText().toString();

        int selectedId = radioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);
        gender = radioButton.getText().toString();


        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (name.equals("")) {
            shouldCancelSignUp = true;
            focusView = etName;
            etName.setError("full name is a required field");
        }
        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = etEmail;
            etEmail.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            shouldCancelSignUp = true;
            focusView = etEmail;
            etEmail.setError("email is not valid");
        }
        if (password.equals("")) {
            shouldCancelSignUp = true;
            focusView = etPassword;
            etPassword.setError("password is a required field");
        }

        if (password.length() < 6) {
            shouldCancelSignUp = true;
            focusView = etPassword;
            etPassword.setError("password must be greater than 6 letters");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = etMobile;
            etMobile.setError("mobile no. is a required field");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = etMobile;
            etMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = etMobile;
            etMobile.setError("mobile no. must be of 10 digit");
        }
        if(dob.matches(""))
        {
            shouldCancelSignUp = true;
            focusView = etDOB;
            etDOB.setError("DOB is a required field");
        }
        if (gender.equals("")) {
            Toast.makeText(this, "Please select gender", Toast.LENGTH_SHORT).show();
        }
        if (address.equals("")) {
            shouldCancelSignUp = true;
            focusView = etAddress;
            etAddress.setError("Address is a required field");
        }
        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {

           // Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();

            firebaseLogin(name,email,password,mobile,dob,gender,address);
        }

    }

    private void firebaseLogin(String name, String email, String password, String mobile, String dob, String gender, String address) {
        progressDialog.setMessage("Sign Up...");
        progressDialog.show();
        progressDialog.setCancelable(false);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            Toast.makeText(SignupActivity.this, "Authentication Successfull.", Toast.LENGTH_SHORT).show();
                            String userId = mAuth.getUid();
                            userModel = new UserModel(name,email,password,mobile,address,dob,gender,userId,0);
                            mFireStore.collection(userModel.USERS_COLLECTION).document(userId).set(userModel)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(SignupActivity.this, "data added", Toast.LENGTH_LONG).show();
                                                startActivity(new Intent(SignupActivity.this, AdminMainActivity.class));
                                            } else {
                                                Toast.makeText(SignupActivity.this, "data not added", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });

                        } else {
                            progressDialog.dismiss();
                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                Toast.makeText(SignupActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


}
