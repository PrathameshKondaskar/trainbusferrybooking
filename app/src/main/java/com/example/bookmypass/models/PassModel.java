package com.example.bookmypass.models;

public class PassModel {
    public static final String PASS_COLLECTION = "Passes";
    String userId,passId,source,destination,type,fromDate,toDate,passengers,time,passName,via;
    int totalFare;
    String timeStamp;

    public PassModel(String userId, String passId, String source, String destination, String type, String fromDate, String toDate, String passengers, String time, int totalFare,String passName,String via,String timeStamp) {
        this.userId = userId;
        this.passId = passId;
        this.source = source;
        this.destination = destination;
        this.type = type;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.passengers = passengers;
        this.time = time;
        this.totalFare = totalFare;
        this.passName = passName;
        this.via = via;
        this.timeStamp = timeStamp;
    }

    public PassModel() {
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getPassName() {
        return passName;
    }

    public void setPassName(String passName) {
        this.passName = passName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassId() {
        return passId;
    }

    public void setPassId(String passId) {
        this.passId = passId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(int totalFare) {
        this.totalFare = totalFare;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
