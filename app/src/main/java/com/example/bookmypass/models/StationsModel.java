package com.example.bookmypass.models;

import java.util.List;

public class StationsModel {

    public static final String STATION_COLLECTION = "Stations";


    String route ;
    List<String> TrainStations;

    public StationsModel(String route, List<String> trainStations) {
        this.route = route;
        TrainStations = trainStations;
    }

    public StationsModel() {
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public List<String> getTrainStations() {
        return TrainStations;
    }

    public void setTrainStations(List<String> trainStations) {
        TrainStations = trainStations;
    }
}
