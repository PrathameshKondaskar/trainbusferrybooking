package com.example.bookmypass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyWalletActivity extends AppCompatActivity {

    //Views
    private TextView tvUserName,tvWallet;
    private Button buttonAddMoney;

    UserSharedPreference userSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        userSharedPreference = new UserSharedPreference(this);

        tvUserName = findViewById(R.id.tvUsername);
        tvWallet = findViewById(R.id.tvWallet);
        buttonAddMoney = findViewById(R.id.buttonAddMoney);

        tvUserName.setText(userSharedPreference.getUsername());
        tvWallet.setText("Rs. "+userSharedPreference.getWallet());

        buttonAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MyWalletActivity.this,PaymentActivity.class));
                finish();
            }
        });
    }
}
