package com.example.bookmypass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bookmypass.models.PassModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PassDetailActivity extends AppCompatActivity {

    //View
    TextView tvSource,tvDestination,tvDate,tvMonthDay,tvName,tvFare,tvFromDate,tvToDate,tvVia,tvTime,tvPassId,tvType;
    ImageView imageView;
    ArrayList<PassModel> passModelArrayList;
    UserSharedPreference userSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_detail);
        passModelArrayList = new ArrayList<>();
        userSharedPreference = new UserSharedPreference(this);

        Intent intent = getIntent();

        String data = intent.getStringExtra("DATA");

        Gson gson = new Gson();
       // passModelArrayList = gson.fromJson(data, new TypeToken<ArrayList<PassModel>>() {}.getType());

        PassModel passModel = gson.fromJson(getIntent().getStringExtra("DATA"), PassModel.class);


        tvSource = findViewById(R.id.tvSource);
        tvDestination = findViewById(R.id.tvDestination);
        tvDate = findViewById(R.id.tvDate);
        tvMonthDay = findViewById(R.id.tvMonthDay);
        tvName = findViewById(R.id.tvName);
        tvFare = findViewById(R.id.tvFare);
        tvFromDate = findViewById(R.id.tvFromDate);
        tvToDate = findViewById(R.id.tvToDate);
        tvVia = findViewById(R.id.tvVia);
        tvTime = findViewById(R.id.tvTime);
        tvPassId = findViewById(R.id.tvPassId);
        imageView = findViewById(R.id.imageView);
        tvType = findViewById(R.id.tvType);


        String  date = passModel.getFromDate();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
        Date dt1= null;
        try {
            dt1 = format1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(dt1);

        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String dayof_month = outFormat.format(dt1);

        SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
        String month = monthFormat.format(dt1);

        SimpleDateFormat YearFormat = new SimpleDateFormat("yyyy");
        String year = YearFormat.format(dt1);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        String currentDate = dateFormat.format(dt1);

        String logo = passModel.getPassName();

        if(logo.matches("BUS")){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_bus_red_24dp));
        }
        if(logo.matches("TRAIN")){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_train_red_24dp));
        }
        if(logo.matches("FERRY")){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_boat_red_24dp));
        }


        tvSource.setText(passModel.getSource());
        tvDestination.setText(passModel.getDestination());
        tvFromDate.setText(passModel.getFromDate());
        tvFare.setText("Rs."+passModel.getTotalFare());
        tvName.setText(userSharedPreference.getUsername());
        tvTime.setText(passModel.getTime());
        tvPassId.setText(passModel.getPassId());
        tvToDate.setText(passModel.getToDate());
        tvType.setText(passModel.getType());


        tvDate.setText(currentDate+""+getDayNumberSuffix(Integer.parseInt(currentDate)));
        tvMonthDay.setText(month+","+dayof_month);

        if(passModel.getVia().matches("")){
            tvVia.setText("--");
        }else {
            tvVia.setText(passModel.getVia());
        }
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

}
