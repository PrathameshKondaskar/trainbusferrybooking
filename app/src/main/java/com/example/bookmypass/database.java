package com.example.bookmypass;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class database extends SQLiteOpenHelper {
    private static final int db_ver = 1;
    SQLiteDatabase sqLiteDatabase;
    private static String db_name = "TRANSPORT.db";

    //region  Columns
    private static String tab_name1 = "bus_stops";
    private static String col_name1 = "stop_id";
    private static String col_name2 = "stop_name";
    private static String col_name3 = "area_id";
    private static String col_name4 = "stop_no";
    private static String col_name5 = "bus_no";

    private static String tab_name2 = "bus_areas";
    private static String col_name21 = "area_id";
    private static String col_name22 = "area_name";

    private static String tab_name4 = "bus_details";
    private static String col_name41 = "bus_id";
    private static String col_name42 = "bus_no";
    private static String col_name43 = "stop_id";


    private static String create_query = "create table " + tab_name1 + "(" +
            col_name1 + " integer primary key autoincrement," +
            col_name2 + " varchar(250)," +
            col_name3 + " varchar(10)," +
            col_name4 + " varchar(10)," +
            col_name5 + " varchar(10))";

    private static String create_query1 = "create table " + tab_name2 + "(" +
            col_name21 + " integer primary key," +
            col_name22 + " varchar(250))";


    private static String create_query3 = "create table " + tab_name4 + "(" +
            col_name41 + " integer primary key autoincrement," +
            col_name42 + " varchar(20)," +
            col_name43 + " varchar(10))";


    public database(Context context) {
        super(context, db_name, null, db_ver);
        getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // sqLiteDatabase =this.getWritableDatabase();
        sqLiteDatabase.execSQL(create_query);
        sqLiteDatabase.execSQL(create_query1);
        sqLiteDatabase.execSQL(create_query3);

    }

    public void insertrawdata() {
        sqLiteDatabase = this.getWritableDatabase();

        rawdata r = new rawdata();
        for (String x : r.areas) {
            sqLiteDatabase.execSQL("insert into " + tab_name2 + " (" + col_name21 + "," + col_name22 + ") values(" + x + ")");
        }
        for (String x : r.stops) {
            sqLiteDatabase.execSQL("insert into " + tab_name1 + " (" + col_name2 + "," + col_name3 + "," + col_name4 + "," + col_name5 + ") values(" + x + ")");
        }
    }

    public ArrayList<String> getAreas() {
        sqLiteDatabase = this.getReadableDatabase();
        ArrayList<String> val = new ArrayList<>();
        Cursor c = sqLiteDatabase.rawQuery("select " + col_name22 + " from " + tab_name2, null);
        if (c.moveToFirst()) {
            do {
                val.add(c.getString(0));
            } while (c.moveToNext());
        }
        for (String x : val) {
            Log.d("Areas", x);
        }
        return val;
    }

    public ArrayList<String> getSourceStops(String source) {
        sqLiteDatabase = this.getReadableDatabase();
        ArrayList<String> val = new ArrayList<>();
        if (source.matches("Sewree")) {

            Cursor c = sqLiteDatabase.rawQuery("select " + col_name2 + " from " + tab_name1 + " where " + col_name3 + "=" +
                    "(select " + col_name21 + " from " + tab_name2 + " where " + col_name22 + "='" + source + "')", null);
            if (c.moveToFirst()) {
                do {
                    val.add(c.getString(0));
                } while (c.moveToNext());
            }
        } else {
            Cursor c = sqLiteDatabase.rawQuery("select " + col_name2 + " from " + tab_name1 + " where " + col_name3 + "=" +
                    "(select " + col_name21 + " from " + tab_name2 + " where " + col_name22 + "='" + source + "')", null);
            if (c.moveToFirst()) {
                do {
                    val.add(c.getString(0));
                } while (c.moveToNext());
            }
        }
        Set<String> list = new LinkedHashSet<>();
        list.addAll(val);
        val.clear();
        val.addAll(list);
        return val;

    }

    public ArrayList<String> getDestStops(String src) {
        sqLiteDatabase = this.getReadableDatabase();
        ArrayList<String> val = new ArrayList<>();
        ArrayList<String> stps = new ArrayList<>();

        Cursor c = sqLiteDatabase.rawQuery("select " + col_name5 + " from " + tab_name1 + " where " + col_name2 + "='" + src + "'", null);
        if (c.moveToFirst()) {
            do {
                String busno = c.getString(0);
                Log.d("Bus No Dest", busno);
                Cursor c1 = sqLiteDatabase.rawQuery("select " + col_name2 + " from " + tab_name1 + " where " + col_name5 + "='" + busno + "'", null);
                if (c1.moveToFirst()) {
                    do {
                        val.add((c1.getString(0)));
                    } while ((c1.moveToNext()));
                }
            } while ((c.moveToNext()));


        }

        Set<String> list = new LinkedHashSet<>();
        list.addAll(val);
        stps.clear();
        stps.addAll(list);


        return stps;


    }

    public ArrayList<String> getDestAreas(String src_stp) {
        sqLiteDatabase = this.getReadableDatabase();
        ArrayList<String> val = new ArrayList<>();
        ArrayList<String> area = new ArrayList<>();

        Cursor c = sqLiteDatabase.rawQuery("select " + col_name5 + " from " + tab_name1 + " where " + col_name2 + "='" + src_stp + "'", null);
        if (c.moveToFirst()) {
            do {
                String busno = c.getString(0);
                Log.d("Stop1 ", src_stp + "  ");
                Log.d("Count", c.getCount() + "");
                Log.d("Bus No", busno);
                Cursor c1 = sqLiteDatabase.rawQuery("select " + col_name3 + " from " + tab_name1 + " where " + col_name5 + "='" + busno + "'", null);
                if (c1.moveToFirst()) {
                    do {
                        // Log.d("Count2",(c1+1)+"");
                        val.add((c1.getString(0)));
                    } while ((c1.moveToNext()));
                }
            } while ((c.moveToNext()));


        }
        for (String x : val) {
            c = sqLiteDatabase.rawQuery("select " + col_name22 + " from " + tab_name2 + " where " + col_name21 + "='" + x + "'", null);
            if (c.moveToFirst()) {
                do {
                    area.add((c.getString(0)));
                } while ((c.moveToNext()));
            }
        }
        Set<String> list = new LinkedHashSet<>();
        list.addAll(area);
        area.clear();
        area.addAll(list);


        return area;

    }

    //endregion
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /*public String getRouteDetails(String route_source, String route_destination) {
        sqLiteDatabase = this.getReadableDatabase();
        String x="",y="",fare="";
        Cursor c = sqLiteDatabase.rawQuery("select "+col_name4+" from "+tab_name1+" where "+col_name2+"='"+route_source+"'",null);
        if(c.moveToFirst())
            x=c.getString(0);
        c = sqLiteDatabase.rawQuery("select "+col_name4+" from "+tab_name1+" where "+col_name2+"='"+route_destination+"'",null);
        if(c.moveToFirst())
            y=c.getString(0);
        if(x.matches(y)){
            c = sqLiteDatabase.rawQuery("select "+col_name35+" from "+tab_name3+" where "+col_name32+"='"+x+"'",null);
            if(c.moveToFirst())
                fare=c.getString(0);

            return "The Route "+x+" is suitable for travel and the monthly pass would be "+fare+"/-\nQuarterly:"+(Integer.parseInt(fare)*3);
        }else
            return "No Buses available for this route";


    }*/

    public int getCount() {
        sqLiteDatabase = this.getReadableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("select * from " + tab_name1, null);
        if (c.moveToFirst())
            return 0;
        else
            return 1;
    }


    public int getstopno(String dt_stp) {
        sqLiteDatabase = this.getReadableDatabase();
        Cursor c = sqLiteDatabase.rawQuery("select " + col_name4 + " from " + tab_name1 + " where " + col_name2 + "='" + dt_stp + "'", null);
        if (c.moveToFirst())
            return Integer.parseInt(c.getString(0));
        else
            return 0;
    }

    public ArrayList<String> getDestStops(String s, ArrayList<String> deststops) {
        sqLiteDatabase = this.getReadableDatabase();
        ArrayList<String> stps = new ArrayList<>();

        Cursor c = sqLiteDatabase.rawQuery("select " + col_name2 + " from " + tab_name1 + " where " + col_name3 + "=(select " + col_name21 + " from " + tab_name2 + " where " + col_name22 + "='" + s + "')", null);
        if (c.moveToFirst()) {
            do {
             //   String area = c.getString(0);
              //  Cursor c1 = sqLiteDatabase.rawQuery("select " + col_name2 + " from " + tab_name1 + " where " + col_name3 + "='" + area + "'", null);
               // if (c1.moveToFirst()) {
              //      do {
                        stps.add((c.getString(0)));
                //    } while ((c1.moveToNext()));
                //}
            } while ((c.moveToNext()));
        }
        Log.d("Size", deststops.size()+"STP"+stps.size() + "");
        for (String c5 : stps) {
            Log.d("stp", c5);
        }
        ArrayList<String> val = new ArrayList<>();
        for (String x : deststops) {
            boolean check = false;
            for (String y : stps) {
                if (x.matches(y)) {
                    val.add(x);
                  //  Log.d("Remove",x);
                }
            }

        }
        Set<String> list = new LinkedHashSet<>();
        list.addAll(val);
        val.clear();
        val.addAll(list);



        return val;
    }
}
