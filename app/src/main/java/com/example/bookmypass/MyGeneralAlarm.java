package com.example.bookmypass;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.bookmypass.models.PassModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MyGeneralAlarm extends BroadcastReceiver {

    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;

    public static Context context;
    ArrayList<PassModel> passModelArrayList;

    String userId;
    String notificationName="", notificationDescription="";
    String date, time;
    String NOTIFICATION_CHANNEL_ID = "My Notification";
    MediaPlayer mediaPlayer;


    ArrayList<String> nameList = new ArrayList<>();
    ArrayList<String> descList = new ArrayList<>();
    ArrayList<String> dateList = new ArrayList<>();
    ArrayList<String> timeList = new ArrayList<>();


    PendingIntent pIntent;

    @Override
    public void onReceive(final Context context, Intent intent) {


        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();
if(user!=null)
{
        userId = user.getUid();


        getDataFromFireBase();

    }
            mediaPlayer = MediaPlayer.create(context, R.raw.ringtone);


      //  addNotification();
//        Intent intent1 = new Intent(context, GeneralReminderActivity.class);
//      pIntent = PendingIntent.getActivity(context, 0, intent1,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//// Create Notification Manager
//        NotificationManager notificationmanager = (NotificationManager) context
//                .getSystemService(NOTIFICATION_SERVICE);
//
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////
////            /* Create or update. */
////            NotificationChannel channel = new NotificationChannel("my_channel_01",
////                    "Channel human readable title",
////                    NotificationManager.IMPORTANCE_DEFAULT);
////            notificationmanager.createNotificationChannel(channel);
////        }
//
//
//        String CHANNEL_ID = "my_channel_01";// The id of the channel.
//        CharSequence name = "Channel human readable title";// The user-visible name of the channel.
//        int importance = NotificationManager.IMPORTANCE_HIGH;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
//        }
//        // Create Notification using NotificationCompat.Builder
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(
//                context)
//                // Set Icon
//                .setSmallIcon(R.mipmap.ic_launcher)
//                // Set Ticker Message
//                .setTicker("hello")
//                // Set Title
//                .setContentTitle(notificationName)
//                // Set Text
//                .setContentText(notificationDescription)
//                // Add an Action Button below Notification
//                .addAction(R.mipmap.ic_launcher, "Action Button", pIntent)
//                // Set PendingIntent into Notification
//                .setContentIntent(pIntent)
//                // Dismiss Notification
//                .setAutoCancel(true)
//                .setChannelId(CHANNEL_ID)
//                .setDefaults(NotificationCompat.DEFAULT_VIBRATE);
//
//
//        // Build Notification with Notification Manager
//        notificationmanager.notify(0, builder.build());

    }

    public void getDataFromFireBase() {
        mFirestore.collection(PassModel.PASS_COLLECTION)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            Log.d("TAG", task.getResult().isEmpty() + " => ");
                            passModelArrayList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if (userId.matches((String) document.getData().get("userId"))) {
                                    PassModel passModel = document.toObject(PassModel.class);
                                    passModelArrayList.add(passModel);
                                    //Toast.makeText(context, " data retrieved", Toast.LENGTH_SHORT).show();
                                }

                            }

                            for (int i = 0; i < passModelArrayList.size(); i++) {
                                String name = passModelArrayList.get(i).getPassName();
                                String desc = passModelArrayList.get(i).getToDate();


                                nameList.add(name);
                                descList.add(desc);

//                                Log.d("Name", i + " " + name);
//                                Log.d("DATE", i + " " + tarikh);
//                                Log.d("TIME", i + " " + vel);

                            }
//                                Intent intent = new Intent(GeneralReminderActivity.this,MyAlarm.class);
//                                intent.putExtra("Date",tarikhlist);
//                                intent.putExtra("Time",vellist);
//                                intent.putExtra("Name",name);


                            Log.d("SIZE", String.valueOf(dateList.size()));
//                            for (int j = 0; j < dateList.size(); j++) {
//                                Log.d("j", String.valueOf(dateList.get(j)));
//                                if (dateList.get(j).matches(date)) {
//                                    Log.d("DateList", dateList.get(j));
//                                    Log.d("Dates", date);
//                            for (int j = 0; j < nameList.size(); j++) {
//
//                                if (timeList.get(j).matches(time)) {
//                                    notificationName = nameList.get(j);
//                                    Log.d("NAME", notificationName);
//                                    notificationDescription = descList.get(j);
//                                    Log.d("NAME1", notificationDescription);
//                                    Log.d("TAG", "hade ye");
//                                    addNotification();
//
//                                }
//                            }
                                //}
                           // }

                            addNotification();
                        } else {

                        }
                    }
                });


    }


    private void addNotification() {
        Log.d("Notifyy","vaj bala vaj");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(notificationName)
                        .setContentText(notificationDescription);
        Intent notificationIntent = new Intent(context, AdminMainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        // Add as notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel nChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
            nChannel.enableLights(true);
            assert manager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            manager.createNotificationChannel(nChannel);
        }
        assert manager != null;
        manager.notify(0, builder.build());
        Log.d("Notify","vaj bala vaj");
        mediaPlayer.start();





//        mediaPlayer.start();
//        Intent intent1 = new Intent(context, HomeActivity.class);
//        intent1.putExtra("MusicOff","musicOff");
//        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent1,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//// Create Notification Manager
//        NotificationManager notificationmanager = (NotificationManager) context
//                .getSystemService(NOTIFICATION_SERVICE);
////
//
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////
////            /* Create or update. */
////            NotificationChannel channel = new NotificationChannel("my_channel_01",
////                    "Channel human readable title",
////                    NotificationManager.IMPORTANCE_DEFAULT);
////            notificationmanager.createNotificationChannel(channel);
////        }
////
//        String CHANNEL_ID = "my_channel_01";// The id of the channel.
//        CharSequence name = "Channel human readable title";// The user-visible name of the channel.
//        int importance = NotificationManager.IMPORTANCE_HIGH;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
//        }
//        // Create Notification using NotificationCompat.Builder
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(
//                context)
//                // Set Icon
//                .setSmallIcon(R.mipmap.ic_launcher)
//                // Set Ticker Message
//                .setTicker("hello")
//                // Set Title
//                .setContentTitle(notificationName)
//                // Set Text
//                .setContentText(notificationDescription)
//                // Add an Action Button below Notification
//                .addAction(R.mipmap.ic_launcher, "Action Button", pIntent)
//                // Set PendingIntent into Notification
//                .setContentIntent(pIntent)
//                // Dismiss Notification
//                .setAutoCancel(true)
//                .setChannelId(CHANNEL_ID)
//                .setDefaults(NotificationCompat.DEFAULT_VIBRATE);
//
//
//
//        // Build Notification with Notification Manager
//        notificationmanager.notify(0, builder.build());

    }
}
