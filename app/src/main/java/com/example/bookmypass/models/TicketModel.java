package com.example.bookmypass.models;

public class TicketModel {

    public static final String _COLLECTION = "Passes";
    String userId,passId,source,destination,time,date,passenger;
    int totalFare;

    public TicketModel(String userId, String passId, String source, String destination, String time, String date, String passenger, int totalFare) {
        this.userId = userId;
        this.passId = passId;
        this.source = source;
        this.destination = destination;
        this.time = time;
        this.date = date;
        this.passenger = passenger;
        this.totalFare = totalFare;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassId() {
        return passId;
    }

    public void setPassId(String passId) {
        this.passId = passId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPassenger() {
        return passenger;
    }

    public void setPassenger(String passenger) {
        this.passenger = passenger;
    }

    public int getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(int totalFare) {
        this.totalFare = totalFare;
    }
}
