package com.example.bookmypass;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.bookmypass.models.BusStopsModel;
import com.example.bookmypass.models.StationsModel;
import com.example.bookmypass.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class AdminMainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    Button btnAddTrain ,btnAddBus,btnAddFerry;
    CardView cardBus,cardTrain,cardFerry;

    //Firebase
    FirebaseFirestore mFirestore;
    StationsModel stationsModel;
    BusStopsModel busStopsModel;

    //Variables
    ArrayList<String> stations;
    ArrayList<StationsModel> stationsList ;
    ArrayList<String> busStops;
    ArrayList<BusStopsModel> busStopsList;
    UserSharedPreference userSharedPreference;
    String userId;
    int wallet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userSharedPreference = new UserSharedPreference(this);

        mFirestore = FirebaseFirestore.getInstance();
        userId = FirebaseAuth.getInstance().getUid();

        btnAddBus = findViewById(R.id.btnAddBus);
        btnAddTrain = findViewById(R.id.btnAddTrain);
        btnAddFerry = findViewById(R.id.btnAddFerry);
        cardBus = findViewById(R.id.cardBus);
        cardFerry = findViewById(R.id.cardFerry);
        cardTrain = findViewById(R.id.cardTrain);
        getWalletFromFirebase();
        cardTrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminMainActivity.this,TrainAddActivity.class));

            }
        });

        cardBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminMainActivity.this,BusAddActivity.class));

            }
        });

        cardFerry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminMainActivity.this,FerreyAddActivity.class));

            }
        });

        //Train Add
        stations = new ArrayList<>();
        stationsList = new ArrayList<>();

        stations.add("CHURCHGATE");
        stations.add("MARINE LINES");
        stations.add("CHARNI ROAD");
        stations.add("GRANT ROAD");
        stations.add("MUMBAI CENTRAL");
        stations.add("MAHALAXMI");
        stations.add("LOWER PAREL");
        stations.add("PRABHADEVI");
        stations.add("DADAR");
        stations.add("MATUNGA ROAD");
        stations.add("MAHIM");
        stations.add("BANDRA");
        stations.add("SANTACRUZ");
        stations.add("VILEPARLE");
        stations.add("ANDHERI");
        stations.add("JOGESHWARI");
        stations.add("RAM MANDIR");
        stations.add("GOREGAON");
        stations.add("KANDIVLI");
        stations.add("BORIVALI");

        stationsList.add(new StationsModel("W",stations));
//        stationsList.add(new StationsModel(1,"CST","H","S"));
//        stationsList.add(new StationsModel(2,"Masjid","H","S"));
//        stationsList.add(new StationsModel(3,"Sandgurst Road","H","S"));
//        stationsList.add(new StationsModel(4,"Dockyard Road","H","S"));

      // addStationsToFirebase();

        //Bus Add
        busStops = new ArrayList<>();
        busStopsList = new ArrayList<>();

        busStops.add("RC CHURCH");
        busStops.add("INS ASHWINI HOSPITAL");
        busStops.add("PILOT BANDAR");
        busStops.add("AFGAN CHURCH");
        busStops.add("COLABA BUS STATION");
        busStops.add("SASOON DOCK");
        busStops.add("STRAND CINEMA");


        busStopsList.add(new BusStopsModel("1",busStops));
        //addBusStopsToFirebase();


        btnAddBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminMainActivity.this,BusAddActivity.class));
            }
        });

        btnAddTrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminMainActivity.this,TrainAddActivity.class));
            }
        });

        btnAddFerry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AdminMainActivity.this,FerreyAddActivity.class));
            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }



    private void addStationsToFirebase() {

        for(int i =0 ; i <stationsList.size();i++) {

            mFirestore.collection(StationsModel.STATION_COLLECTION).add(stationsList.get(i))
                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {

                            if (task.isSuccessful()) {

                                Toast.makeText(AdminMainActivity.this, "Station Added", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(AdminMainActivity.this, (CharSequence) task.getException(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AdminMainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void addBusStopsToFirebase(){
        mFirestore.collection(BusStopsModel.BUSSTOPS_COLLECTION).add(busStopsList.get(0))
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {

                            Toast.makeText(AdminMainActivity.this, "Busstops Added", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AdminMainActivity.this, (CharSequence) task.getException(), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AdminMainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.action_settings:
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(this,LoginActivity.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void getWalletFromFirebase(){

        mFirestore.collection(UserModel.USERS_COLLECTION)
                .whereEqualTo("id",userId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful()) {


                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                                UserModel userModel = documentSnapshot.toObject(UserModel.class);
                                wallet = userModel.getWallet();
                                userSharedPreference.setWallet(wallet);
                                userSharedPreference.setUsername(userModel.getName());
                            }

                        }


                    }
                });

    }
}

