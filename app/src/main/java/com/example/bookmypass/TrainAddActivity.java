package com.example.bookmypass;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.content.res.ResourcesCompat;

import com.example.bookmypass.models.PassModel;
import com.example.bookmypass.models.StationsModel;
import com.example.bookmypass.models.TrainModel;
import com.example.bookmypass.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class TrainAddActivity extends AppCompatActivity {

    //Views
    private EditText etTime ;
    private AutoCompleteTextView etSource,etDestination;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button buttonSubmit,btnCalc;
    private AppCompatSpinner spinner;
    private TextView tvFromDate ,tvToDate,tvFare;
    private LinearLayout linearVia;

    //Variables
    String passId, route, source, destination, time;
    String sourceName,destinationName;
    ArrayList<String> destinationList,sourceList;
    ArrayList<StationsModel> stationsModels;
    ArrayList<String> passTypeList;
    UserSharedPreference userSharedPreference;
    String via="" ;

    ArrayList<StationsModel> wStationlist,cStationlist,hStationlist;
    int wallet =0;
    String passType="";
    String  month ,day;
    int fare;
    //Firebase
    FirebaseFirestore mFirestore;
    FirebaseAuth mAuth;

    int sourcePosition,destinationPosition;
    int stationCountSource ,stationcountDestination,totalStation = 0;
    int totalFare;
    int flagForWestern = 0;
    String userId;
    Calendar c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_add);

        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        userId = mAuth.getUid();

        userSharedPreference = new UserSharedPreference(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      //  etTime = findViewById(R.id.etTime);
        tvFromDate = findViewById(R.id.tvFromDate);
        tvToDate = findViewById(R.id.tvToDate);
        etSource = findViewById(R.id.etSource);
        etDestination = findViewById(R.id.etDestination);
        radioGroup = findViewById(R.id.radioGroup);
        buttonSubmit = findViewById(R.id.buttonSubmit);
        spinner = findViewById(R.id.spinnerPassType);
       // btnCalc = findViewById(R.id.btnCalc);
        linearVia = findViewById(R.id.linearVia);
        tvFare = findViewById(R.id.tvFare);

        spinner.setPrompt("here");
        if(flagForWestern==0){
            getStations("W");
            route = "Western";
        }
//
//        btnCalc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(route.matches("Western")){
//
//                    if(destinationPosition > 19){
//                        stationCountSource = sourcePosition - 8;
//
//                        stationcountDestination = (destinationPosition-20) - 7;
//
//                        totalStation = Math.abs(stationCountSource)+Math.abs(stationcountDestination);
//
//                        Toast.makeText(TrainAddActivity.this, "count"+totalStation, Toast.LENGTH_SHORT).show();
//
//                        System.out.println("via_s_count "+totalStation);
//                    }
//                    else{
//                        stationCountSource = sourcePosition;
//                        stationcountDestination = destinationPosition;
//                        totalStation = destinationPosition - sourcePosition;
//
//                        Toast.makeText(TrainAddActivity.this, "count"+totalStation, Toast.LENGTH_SHORT).show();
//
//                        System.out.println("s_sount"+Math.abs(totalStation));
//
//                        System.out.println("w");
//                    }
//
//                }
//
//                if(route.matches("Central")){
//
//                    if(destinationPosition < 19){
//                        stationCountSource = sourcePosition - 7;
//
//                        stationcountDestination = 8 - destinationPosition;
//
//                        totalStation = Math.abs(stationCountSource)+Math.abs(stationcountDestination);
//
//                        Toast.makeText(TrainAddActivity.this, "count"+totalStation, Toast.LENGTH_SHORT).show();
//
//                        System.out.println("via_s_count "+totalStation);
//                    }
//                    else{
//                        stationCountSource = sourcePosition;
//                        stationcountDestination = destinationPosition;
//                        totalStation = destinationPosition - sourcePosition;
//                        Toast.makeText(TrainAddActivity.this, "count"+totalStation, Toast.LENGTH_SHORT).show();
//                        System.out.println("s_sount"+Math.abs(totalStation));
//
//                        System.out.println("w");
//                    }
//
//                }
//
//            }
//        });

        wStationlist = new ArrayList<>();
        cStationlist = new ArrayList<>();
        hStationlist = new ArrayList<>();
        passTypeList = new ArrayList<>();

        passTypeList.add("Select");
        passTypeList.add("Monthly");
        passTypeList.add("Quarterly");
        CustomAdapter customAdapter = new CustomAdapter(TrainAddActivity.this, android.R.layout.simple_list_item_1,passTypeList);
        spinner.setAdapter(customAdapter);

        destinationList = new ArrayList<>();
        sourceList = new ArrayList<>();


        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String date = df.format(Calendar.getInstance().getTime());

        tvFromDate.setText(date);



        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                int posi = spinner.getSelectedItemPosition();

                if(!etSource.getText().toString().matches("") && !etDestination.getText().toString().matches("")
                        && checkSourceStation() && checkDestinationStation() &&
                        !etSource.getText().toString().matches(etDestination.getText().toString())) {
                    spinner.setEnabled(true);


                    if (posi == 1) {
                        passType = "Monthly";

                        c = Calendar.getInstance();

                        // add months to current date
                        c.add(Calendar.MONTH, 1);


                        if (c.get(Calendar.MONTH) < 10) {

                            month = "0" + (c.get(Calendar.MONTH) + 1);
                        } else {
                            month = (c.get(Calendar.MONTH)) + "";
                        }

                        if (c.get(Calendar.DATE) < 10) {

                            day = "0" + (c.get(Calendar.DATE) - 1);
                        } else {
                            day = (c.get(Calendar.DATE) - 1) + "";
                        }

                        tvToDate.setText(day + "/" + month + "/" + c.get(Calendar.YEAR));


                        if (route.matches("Western")) {

                            if (destinationPosition > 19) {
                                stationCountSource = sourcePosition - 8;

                                stationcountDestination = (destinationPosition - 20) - 7;

                                totalStation = Math.abs(stationCountSource) + Math.abs(stationcountDestination);

                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();

                                System.out.println("via_s_count " + totalStation);

                                linearVia.setVisibility(View.VISIBLE);
                                via ="DADAR" ;

                                totalFare = calculateFare(1);

                                tvFare.setText("Rs." + totalFare);

                            } else {
                                stationCountSource = sourcePosition;
                                stationcountDestination = destinationPosition;
                                totalStation = destinationPosition - sourcePosition;

                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();

                                System.out.println("s_sount" + Math.abs(totalStation));

                                System.out.println("w");

                                linearVia.setVisibility(View.GONE);
                                via = "";

                                totalFare = calculateFare(1);

                                tvFare.setText("Rs." + totalFare);
                            }

                        }

                        if (route.matches("Central")) {

                            if (destinationPosition < 19) {
                                stationCountSource = sourcePosition - 7;

                                stationcountDestination = 8 - destinationPosition;

                                totalStation = Math.abs(stationCountSource) + Math.abs(stationcountDestination);

                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();

                                System.out.println("via_s_count " + totalStation);

                                linearVia.setVisibility(View.VISIBLE);
                                via = "DADAR";

                                totalFare = calculateFare(1);

                                tvFare.setText("Rs." + totalFare);
                            } else {
                                stationCountSource = sourcePosition;
                                stationcountDestination = destinationPosition;
                                totalStation = destinationPosition - sourcePosition;
                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();
                                System.out.println("s_sount" + Math.abs(totalStation));

                                linearVia.setVisibility(View.GONE);
                                via = "";
                                System.out.println("w");
                                totalFare = calculateFare(1);

                                tvFare.setText("Rs." + totalFare);
                            }

                        }

                        if (route.matches("Harbour")) {
                            stationCountSource = sourcePosition;
                            stationcountDestination = destinationPosition;
                            totalStation = Math.abs(destinationPosition - sourcePosition);
                            linearVia.setVisibility(View.GONE);
                            via = "";

                            totalFare = calculateFare(1);

                            tvFare.setText("Rs." + totalFare);
                        }


                    }
                    if (posi == 2) {
                        passType = "Quarterly";

                         c = Calendar.getInstance();

                        // add months to current date
                        c.add(Calendar.MONTH, 3);


                        if (c.get(Calendar.MONTH) < 10) {

                            month = "0" + (c.get(Calendar.MONTH) + 1);
                        } else {
                            month = (c.get(Calendar.MONTH)) + "";
                        }

                        if (c.get(Calendar.DATE) < 10) {

                            day = "0" + (c.get(Calendar.DATE) - 1);
                        } else {
                            day = (c.get(Calendar.DATE) - 1) + "";
                        }

                        tvToDate.setText(day + "/" + month + "/" + c.get(Calendar.YEAR));

                        if (route.matches("Western")) {

                            if (destinationPosition > 19) {
                                stationCountSource = sourcePosition - 8;

                                stationcountDestination = (destinationPosition - 20) - 7;

                                totalStation = Math.abs(stationCountSource) + Math.abs(stationcountDestination);

                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();

                                System.out.println("via_s_count " + totalStation);

                                linearVia.setVisibility(View.VISIBLE);
                                via = "DADAR";

                                totalFare = calculateFare(3);

                                tvFare.setText("Rs." + totalFare);

                            } else {
                                stationCountSource = sourcePosition;
                                stationcountDestination = destinationPosition;
                                totalStation = destinationPosition - sourcePosition;

                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();

                                System.out.println("s_sount" + Math.abs(totalStation));

                                System.out.println("w");

                                linearVia.setVisibility(View.GONE);

                                via = "";

                                totalFare = calculateFare(3);

                                tvFare.setText("Rs." + totalFare);
                            }

                        }

                        if (route.matches("Central")) {

                            if (destinationPosition < 19) {
                                stationCountSource = sourcePosition - 7;

                                stationcountDestination = 8 - destinationPosition;

                                totalStation = Math.abs(stationCountSource) + Math.abs(stationcountDestination);

                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();

                                System.out.println("via_s_count " + totalStation);

                                linearVia.setVisibility(View.VISIBLE);
                                via = "DADAR";

                                totalFare = calculateFare(3);

                                tvFare.setText("Rs." + totalFare);
                            } else {
                                stationCountSource = sourcePosition;
                                stationcountDestination = destinationPosition;
                                totalStation = destinationPosition - sourcePosition;
                                Toast.makeText(TrainAddActivity.this, "count" + totalStation, Toast.LENGTH_SHORT).show();
                                System.out.println("s_sount" + Math.abs(totalStation));

                                linearVia.setVisibility(View.GONE);
                                via = "";
                                System.out.println("w");
                                totalFare = calculateFare(3);

                                tvFare.setText("Rs." + totalFare);
                            }

                        }

                        if (route.matches("Harbour")) {
                            stationCountSource = sourcePosition;
                            stationcountDestination = destinationPosition;
                            totalStation = Math.abs(destinationPosition - sourcePosition);
                            linearVia.setVisibility(View.GONE);
                            via = "";

                            totalFare = calculateFare(3);

                            tvFare.setText("Rs." + totalFare);
                        }
                    }
                }else {

                    tvToDate.setText("");
                    tvFare.setText("");
                    Toast.makeText(TrainAddActivity.this, "please select right source or Destination ", Toast.LENGTH_SHORT).show();

                }
                //Log.d("Role", passType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        passId = randomNo();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                      radioButton = findViewById(checkedId);
                                                      route = radioButton.getText().toString();
                                                      flagForWestern = 1;

                                                      if (route.matches("Western")) {
                                                          getStations("W");
                                                          etSource.setText("");
                                                          etDestination.setText("");
                                                          tvFare.setText("");
                                                          tvToDate.setText("");
                                                          spinner.setSelection(0);
                                                          linearVia.setVisibility(View.GONE);
                                                          via = "";

                                                      }
                                                      if (route.matches("Central")) {
                                                          getStations("C");
                                                          etSource.setText("");
                                                          etDestination.setText("");
                                                          tvFare.setText("");
                                                          tvToDate.setText("");
                                                          spinner.setSelection(0);
                                                          linearVia.setVisibility(View.GONE);
                                                          via = "";

                                                      }
                                                      if (route.matches("Harbour")) {
                                                          getStations("H");
                                                          etSource.setText("");
                                                          etDestination.setText("");
                                                          tvFare.setText("");
                                                          tvToDate.setText("");
                                                          spinner.setSelection(0);
                                                          linearVia.setVisibility(View.GONE);
                                                          via = "";
                                                      }
                                                  }
                                              }
        );



//
//        etTime.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(TrainAddActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        String AM_PM;
//                        if (selectedHour >=0 && selectedHour < 12){
//                            AM_PM = "AM";
//                        } else {
//                            AM_PM = "PM";
//                        }
//                        etTime.setText( selectedHour + ":" + selectedMinute+" "+AM_PM);
//                    }
//                }, hour, minute, false);
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//
//            }
//        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addNewTrain();
            }
        });

    }

    public boolean checkSourceStation(){


        for (int i =0 ;i<sourceList.size();i++){

            if(etSource.getText().toString().matches(sourceList.get(i))){

                return true;
            }
        }

        return false;
    }


    public boolean checkDestinationStation(){


        for (int i =0 ;i<destinationList.size();i++){

            if(etDestination.getText().toString().matches(destinationList.get(i))){

                return true;
            }
        }

        return false;
    }

    public  int  calculateFare(int type){

        if(totalStation <=5){

            fare = 200 * type;
        }
        else  if(totalStation > 5 && totalStation <=10 ){
            fare = 300 * type;
        }
        else if(totalStation > 10 && totalStation <=15){
            fare = 400 * type;
        }
        else if(totalStation >15 && totalStation <=20){
            fare = 500 * type;
        }
        else {
            fare = 600 * type;
        }

        return  fare;
    }

    private void getStations(final String route) {
        mFirestore.collection(StationsModel.STATION_COLLECTION)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("ERR", "Error:" + e.getMessage());
                        } else {
                            destinationList = new ArrayList<>();
                            stationsModels = new ArrayList<>();
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                StationsModel stationsModel = document.toObject(StationsModel.class);
                                stationsModels.add(stationsModel);
                            }

                            sourceList.clear();
                            destinationList.clear();
                            for(int i = 0 ;i<stationsModels.size();i++){

                                for(int j = 0;j<stationsModels.get(i).getTrainStations().size();j++)
                                {


                                    if(route.matches("W")){

                                        if(route.matches(stationsModels.get(i).getRoute())) {
                                            sourceList.add(stationsModels.get(i).getTrainStations().get(j));

                                            wStationlist.add(stationsModels.get(i));
                                        }

                                        if(!stationsModels.get(i).getRoute().matches("H")){
                                            destinationList.add(stationsModels.get(i).getTrainStations().get(j));
                                        }


                                    }
                                    else if(route.matches("C")){

                                        if(route.matches(stationsModels.get(i).getRoute())) {
                                            sourceList.add(stationsModels.get(i).getTrainStations().get(j));

                                            cStationlist.add(stationsModels.get(i));
                                        }
                                        if(!stationsModels.get(i).getRoute().matches("H")){
                                            destinationList.add(stationsModels.get(i).getTrainStations().get(j));
                                        }
                                    }
                                    else
                                    {
                                        if(route.matches(stationsModels.get(i).getRoute())) {
                                            sourceList.add(stationsModels.get(i).getTrainStations().get(j));
                                            destinationList.add(stationsModels.get(i).getTrainStations().get(j));

                                            hStationlist.add(stationsModels.get(i));
                                        }

                                    }
                                }
                            }

                            final ArrayAdapter<String> DestinationAdapter = new ArrayAdapter<String>(TrainAddActivity.this,android.R.layout.select_dialog_singlechoice, destinationList);

                            final ArrayAdapter<String> SourceAdapter = new ArrayAdapter<String>(TrainAddActivity.this,android.R.layout.select_dialog_singlechoice, sourceList);

                            //Find TextView control
                            //Set the number of characters the user must type before the drop down list is shown
                            //Set the adapter


                            etSource.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    if(sourceList.size()>0) {
                                        etSource.setAdapter(SourceAdapter);
                                        etSource.showDropDown();
                                        etSource.setThreshold(1);
                                    }
                                    else
                                    {
                                        Toast.makeText(TrainAddActivity.this, "please select the route", Toast.LENGTH_SHORT).show();
                                    }


                                    return false;
                                }
                            });

                            etDestination.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    etDestination.setAdapter(DestinationAdapter);
                                    etDestination.showDropDown();
                                    etDestination.setThreshold(1);
                                    return false;
                                }
                            });

                            etSource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                     if(etSource.getText().toString().matches("")) {
                                         sourcePosition = sourceList.indexOf(etSource.getText().toString());
                                     }
                                     else{
                                         tvFare.setText("");
                                         tvToDate.setText("");
                                         spinner.setSelection(0);
                                         linearVia.setVisibility(View.GONE);
                                         via = "";
                                         sourcePosition = sourceList.indexOf(etSource.getText().toString());
                                     }

                                }
                            });

                            etDestination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    if(etDestination.getText().toString().matches("")) {
                                        destinationPosition = destinationList.indexOf(etDestination.getText().toString());
                                    }
                                    else{
                                        tvFare.setText("");
                                        tvToDate.setText("");
                                        spinner.setSelection(0);
                                        linearVia.setVisibility(View.GONE);
                                        via = "";
                                        destinationPosition = destinationList.indexOf(etDestination.getText().toString());
                                    }
                                }
                            });






                        }
                    }
                });

    }

    private void addNewTrain() {

        source = etSource.getText().toString();
        destination = etDestination.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (source.equals("")) {
            shouldCancelSignUp = true;
            focusView = etSource;
            etSource.setError("Source is a required field");
        }
        if (destination.equals("")) {
            shouldCancelSignUp = true;
            focusView = etDestination;
            etDestination.setError("Destination0 is a required field");
        }
        if(passType.matches("")){

            Toast.makeText(this, "please select type", Toast.LENGTH_SHORT).show();
        }
        else if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else{

            System.out.println(userSharedPreference.getWallet());


            if (totalFare > userSharedPreference.getWallet())
            {

                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog .setTitleText("Please Recharge your Wallet...!!!")
                    .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                startActivity(new Intent(TrainAddActivity.this,MyWalletActivity.class));
                                sweetAlertDialog.dismiss();
                            }
                        })
                    .show();

            }else {

                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();

                TrainModel trainModel = new TrainModel(passId, route, time, source, destination);

                PassModel passModel = new PassModel(userId,passId,source,destination,passType,tvFromDate.getText().toString(),tvToDate.getText().toString(),"1",new SimpleDateFormat("H:mm a", Locale.getDefault()).format(new Date()),totalFare,"TRAIN",via,DateFormatHelper.getISOString(new Date()));


                mFirestore.collection(PassModel.PASS_COLLECTION)
                        .add(passModel)
                        .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentReference> task) {
                                if (task.isSuccessful()) {

                                    wallet = userSharedPreference.getWallet() - totalFare ;
                                    userSharedPreference.setWallet(wallet);

                                    Map<String, Object> map = new HashMap<>();
                                    map.put("wallet", wallet);


                                    mFirestore.collection(UserModel.USERS_COLLECTION)
                                            .document(userId)
                                            .update(map)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {

                                                }
                                            });

                                    //set alarm and Notification

//                                    c.set(
//                                            c.get(Calendar.YEAR),
//                                            Integer.parseInt(month),
//                                            Integer.parseInt(day)-2,
//                                            10,
//                                            0,
//                                            0);


//                                    c.set(
//                                            c.get(Calendar.YEAR),
//                                            c.get(Calendar.MONTH)-1,
//                                            c.get(Calendar.DAY_OF_MONTH),
//                                            10,
//                                            55,
//                                            0);

                                    c.set(Calendar.MONTH,c.get(Calendar.MONTH)-1);
                                    c.set(Calendar.DAY_OF_MONTH,Calendar.DAY_OF_MONTH);
                                    c.set(Calendar.HOUR_OF_DAY, 5);
                                    c.set(Calendar.MINUTE, 45);
                                    c.set(Calendar.SECOND, 0);
                                    c.set(Calendar.MILLISECOND, 0);
                                    c.set(Calendar.AM_PM, Calendar.PM);


                                    Log.d("Mili", String.valueOf(c.getTimeInMillis()));
                                   // setAlarm(c.getTimeInMillis());

                                    Toast.makeText(TrainAddActivity.this, "Pass Added", Toast.LENGTH_SHORT).show();

                                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(TrainAddActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                                    sweetAlertDialog .setTitleText("Pass Booked Successfully")
                                            .setConfirmText("Ok")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                                    sweetAlertDialog.dismiss();

                                                    Utility.addNotification(TrainAddActivity.this,"Train Pass Book",source+" To "+destination);

                                                    Gson gson = new Gson();
                                                    String data = gson.toJson(passModel);

                                                    Intent intent =new Intent(TrainAddActivity.this,PassDetailActivity.class);
                                                    intent.putExtra("DATA",data);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            })
                                            .show();



                                } else {
                                    Toast.makeText(TrainAddActivity.this, (CharSequence) task.getException(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(TrainAddActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }


    }

    public  void setAlarm(long timeInMillis) {

        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        ArrayList<PendingIntent> intentArray = new ArrayList<PendingIntent>();


        Intent intent = new Intent(this, MyGeneralAlarm.class);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
        final int i = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, i, intent,
                PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInMillis , AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();

        intentArray.add(pendingIntent);


    }

    public String randomNo() {
        char[] chars1 = "0123456789".toCharArray();
        StringBuilder sb1 = new StringBuilder("T-");
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return sb1.toString();
    }



    public class CustomAdapter extends ArrayAdapter<String> implements SpinnerAdapter {
        Context context;
        ArrayList<String> passType = new ArrayList<>();

        public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.passType = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            View view = super.getView(position, convertView, parent);

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(getResources().getColor(R.color. black));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER);
            Typeface typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_semibold);
            textView.setTypeface(typeface);
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = LayoutInflater.from(TrainAddActivity.this).inflate(
                    R.layout.custom_spinner, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(passType.get(position));

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
