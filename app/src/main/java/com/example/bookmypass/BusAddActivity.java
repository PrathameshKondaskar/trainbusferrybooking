package com.example.bookmypass;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.example.bookmypass.models.PassModel;
import com.example.bookmypass.models.TrainModel;
import com.example.bookmypass.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BusAddActivity extends AppCompatActivity {

    static ArrayList<String> sourceArea, sourcestops, destArea, deststops;
    database db;
    AppCompatSpinner source_area, dest_area, duration;
    AutoCompleteTextView source_stop, dest_stop;
    TextView tvFare ;
    String[] term = {"Term", "Monthly", "Quarterly"};
    Button submit;
    ArrayAdapter<String> adp;
    String src_area = "", src_stp = "";
    String dt_area = "", dt_stp = "";
    int stpsrccount = 0, stpdtcount = 0;
    int PassAmount = 0;
    TextView frmDate, ToDate;
    Date c;
    SimpleDateFormat df,df1;
    String formattedDate,formattedDate1;
    Calendar c1;
    int wallet =0;
    String passId;

    //Firebase
    FirebaseFirestore mFirestore;
    FirebaseAuth mAuth;
    String userId;
    UserSharedPreference userSharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_add);

        userSharedPreference = new UserSharedPreference(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        userId = mAuth.getUid();

        passId = randomNo();
        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("dd/MM/yyyy");


        formattedDate= df.format(c);
        c1 = Calendar.getInstance();


        df1 = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate1 = df1.format(c1.getTime());





        db = new database(this);
        int i = db.getCount();
        if (i == 1)
            db.insertrawdata();
        submit = (Button) findViewById(R.id.view_routes);
        source_area =  findViewById(R.id.source_area);
        source_stop = (AutoCompleteTextView) findViewById(R.id.source_stop);
        dest_area =  findViewById(R.id.dest_area);
        duration =  findViewById(R.id.duration);
        dest_stop = (AutoCompleteTextView) findViewById(R.id.dest_stop);
        frmDate = (TextView) findViewById(R.id.frmDate);
        ToDate = (TextView) findViewById(R.id.toDate);
        tvFare = findViewById(R.id.tvFare);

        frmDate.setText(formattedDate);
        ToDate.setText(formattedDate1);


        sourceArea = db.getAreas();
        destArea = db.getAreas();
        adp = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sourceArea);
        source_area.setAdapter(adp);
        dest_area.setAdapter(adp);
        adp = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, term);
        duration.setAdapter(adp);



        source_stop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                    source_stop.showDropDown();
                    source_stop.setThreshold(1);


                return false;
            }
        });

        dest_stop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                dest_stop.showDropDown();
                dest_stop.setThreshold(1);
                return false;
            }
        });



        source_stop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (src_area == null || src_area == "") {
                    Toast.makeText(BusAddActivity.this, "Please Select Source Area", Toast.LENGTH_SHORT).show();
                    return;
                } else if (src_area != "") {
                    for (String x : sourcestops) {
                        Log.d("Stops", x);
                    }
                    Log.d("Position", source_stop.getText().toString());
                    src_stp = source_stop.getText().toString();
                    destArea = db.getDestAreas(src_stp);
                    destArea.remove(src_area);
                    adp = new ArrayAdapter<>(BusAddActivity.this, android.R.layout.simple_list_item_1, destArea);
                    dest_area.setAdapter(adp);
                    deststops = db.getDestStops(src_stp);
                    for (String c5 : deststops) {
                        Log.d("Dtn", c5);
                    }
                    adp = new ArrayAdapter<>(BusAddActivity.this, android.R.layout.simple_list_item_1, deststops);
                    dest_stop.setAdapter(adp);
                    dest_area.setSelected(false);
                    dest_area.setSelection(0);

                    stpsrccount = db.getstopno(src_stp);
                }
            }
        });

        source_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                src_area = sourceArea.get(i);
                sourcestops = db.getSourceStops(src_area);
                adp = new ArrayAdapter<>(BusAddActivity.this, android.R.layout.simple_list_item_1, sourcestops);
                source_stop.setAdapter(adp);
                destArea.remove(src_area);
                // source_stop.setThreshold(0);
                source_stop.setText("");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*
        source_stop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        dest_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (src_area == null || src_area == "") {
                    Toast.makeText(BusAddActivity.this, "Please Select Source Area", Toast.LENGTH_SHORT).show();
                }
                if (src_stp == null || src_stp == "") {
                    Toast.makeText(BusAddActivity.this, "Please Select Source Stop", Toast.LENGTH_SHORT).show();
                } else {
                    if (deststops != null) {
                        dt_area=dest_area.getSelectedItem().toString();
                        deststops = db.getDestStops(src_stp);
                        Toast.makeText(BusAddActivity.this, "" + destArea.get(i), Toast.LENGTH_SHORT).show();
                        Toast.makeText(BusAddActivity.this, "size" + deststops.size() + "", Toast.LENGTH_SHORT).show();
                        ArrayList<String> temp = db.getDestStops(destArea.get(i), deststops);
                        adp = new ArrayAdapter<>(BusAddActivity.this, android.R.layout.simple_list_item_1, temp);
                        dest_stop.setAdapter(adp);
                        dest_stop.setText("");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dest_stop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dt_stp = dest_stop.getText().toString();
                stpdtcount = db.getstopno(dt_stp);

            }
        });
        duration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (duration.getSelectedItem().toString().matches("Monthly")) {
                    PassAmount = 0;
                    if (stpdtcount > 1 && stpdtcount <= 6) {
                        PassAmount = 150;
                    } else if (stpdtcount > 6 && stpdtcount < 12) {
                        PassAmount = 250;
                    } else if (stpdtcount > 12 && stpdtcount <= 18) {
                        PassAmount = 350;
                    } else if (stpdtcount > 18 && stpdtcount < 24) {
                        PassAmount = 550;
                    } else if (stpdtcount > 24 && stpdtcount <= 30) {
                        PassAmount = 650;
                    } else if (stpdtcount > 30 && stpdtcount < 36) {
                        PassAmount = 750;
                    }
                    if (stpdtcount != 0) {
                        PassAmount = PassAmount * 1;
                    }
                    c1 = Calendar.getInstance();
                    c1.add(Calendar.MONTH,1);
                    formattedDate1=df1.format(c1.getTime());
                    ToDate.setText(formattedDate1);
                } else if (duration.getSelectedItem().toString().matches("Quarterly")) {
                    PassAmount = 0;
                    if (stpdtcount > 1 && stpdtcount <= 6) {
                        PassAmount = 150;
                    } else if (stpdtcount > 6 && stpdtcount < 12) {
                        PassAmount = 250;
                    } else if (stpdtcount > 12 && stpdtcount <= 18) {
                        PassAmount = 350;
                    } else if (stpdtcount > 18 && stpdtcount < 24) {
                        PassAmount = 550;
                    } else if (stpdtcount > 24 && stpdtcount <= 30) {
                        PassAmount = 650;
                    } else if (stpdtcount > 30 && stpdtcount < 36) {
                        PassAmount = 750;
                    }
                    if (stpdtcount != 0) {
                        PassAmount = PassAmount * 3;
                    }
                    c1 = Calendar.getInstance();
                    c1.add(Calendar.MONTH,3);
                    formattedDate1=df1.format(c1.getTime());
                    ToDate.setText(formattedDate1);
                }
               // Toast.makeText(BusAddActivity.this, "" + PassAmount, Toast.LENGTH_SHORT).show();
                tvFare.setText("Rs. "+PassAmount);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(BusAddActivity.this, "Works", Toast.LENGTH_SHORT).show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!src_area.matches("")&&!src_stp.matches("")&&!dt_area.matches("")&&!dt_stp.matches("")&&!duration.getSelectedItem().toString().matches("")
                        &&!formattedDate.matches("")&&!formattedDate1.matches("")&&!(PassAmount+"").matches("0")) {
                    int count = stpsrccount - stpdtcount;
                    Toast.makeText(BusAddActivity.this, "Count : " + count + "\nSource" + stpsrccount + "\nDestination" + stpdtcount, Toast.LENGTH_SHORT).show();
                    String x = "\nSource Area :" + src_area +
                            "\nSource Stop : " + src_stp +
                            "\nDestination Area : " + dt_area +
                            "\nDestination Stop : " + dt_stp +
                            "\nDuration : " + duration.getSelectedItem().toString() +
                            "\nFrom Date : " + formattedDate +
                            "\nTo Date : " + formattedDate1 +
                            "\nPass Amount : " + PassAmount;
                  //  Toast.makeText(BusAddActivity.this, "" + x, Toast.LENGTH_SHORT).show();

                    if (PassAmount > userSharedPreference.getWallet())
                        {

                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(BusAddActivity.this, SweetAlertDialog.WARNING_TYPE);
                            sweetAlertDialog .setTitleText("Please Recharge your Wallet...!!!")
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                                            startActivity(new Intent(BusAddActivity.this,MyWalletActivity.class));
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();

                        }else {

                            Toast.makeText(BusAddActivity.this, "Success", Toast.LENGTH_SHORT).show();


                            PassModel passModel = new PassModel(userId, passId, src_stp, dt_stp, duration.getSelectedItem().toString(),formattedDate, formattedDate1,"1",new SimpleDateFormat("H:mm a", Locale.getDefault()).format(new Date()), PassAmount,"BUS","",DateFormatHelper.getISOString(new Date()));


                            mFirestore.collection(PassModel.PASS_COLLECTION)
                                    .add(passModel)
                                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentReference> task) {
                                            if (task.isSuccessful()) {

                                                wallet = userSharedPreference.getWallet() - PassAmount;
                                                userSharedPreference.setWallet(wallet);

                                                Map<String, Object> map = new HashMap<>();
                                                map.put("wallet", wallet);


                                                mFirestore.collection(UserModel.USERS_COLLECTION)
                                                        .document(userId)
                                                        .update(map)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {

                                                            }
                                                        });

                                                Toast.makeText(BusAddActivity.this, "Pass Added", Toast.LENGTH_SHORT).show();

                                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(BusAddActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                                                sweetAlertDialog.setTitleText("Pass Booked Successfully")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                                                sweetAlertDialog.dismiss();
                                                                Utility.addNotification(BusAddActivity.this,"Bus Pass Booked",src_stp+" To "+dt_stp);

                                                                Gson gson = new Gson();
                                                                String data = gson.toJson(passModel);

                                                                Intent intent =new Intent(BusAddActivity.this,PassDetailActivity.class);
                                                                intent.putExtra("DATA",data);
                                                                startActivity(intent);
                                                                finish();
                                                            }
                                                        })
                                                        .show();
                                            } else {
                                                Toast.makeText(BusAddActivity.this, (CharSequence) task.getException(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(BusAddActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });


                        }
                }else{
                    Toast.makeText(BusAddActivity.this, "Please Fill All Details", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public String randomNo() {
        char[] chars1 = "0123456789".toCharArray();
        StringBuilder sb1 = new StringBuilder("B-");
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return sb1.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
