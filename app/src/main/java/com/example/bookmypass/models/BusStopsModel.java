package com.example.bookmypass.models;

import java.util.List;

public class BusStopsModel {

    public static final String BUSSTOPS_COLLECTION = "BusStops";
    String busNo;
    List<String> busStops;

    public BusStopsModel(String busNo, List<String> busStops) {
        this.busNo = busNo;
        this.busStops = busStops;
    }

    public BusStopsModel() {
    }

    public String getBusNo() {
        return busNo;
    }

    public void setBusNo(String busNo) {
        this.busNo = busNo;
    }

    public List<String> getBusStops() {
        return busStops;
    }

    public void setBusStops(List<String> busStops) {
        this.busStops = busStops;
    }
}
