package com.example.bookmypass;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    //VIEWS
    private Button buttonLogin;
    private TextInputEditText editTextEmail, editTextPassword;
    private TextView tvForgotPassword,buttonSignUp;
    private ProgressDialog progressDialog ;

    //Firebase Variables;
    FirebaseAuth mAuth;
    FirebaseUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();





    }

    private void init() {

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        buttonSignUp =  findViewById(R.id.tvSignup);
        buttonLogin = findViewById(R.id.btnLogin);
        editTextEmail = findViewById(R.id.etEmail);
        editTextPassword =  findViewById(R.id.etPassword);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        progressDialog = new ProgressDialog(this);

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));

            }
        });
        if (user != null) {
            startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
            finish();
        }
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin();

            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });
    }


    public void userLogin() {


        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("Email is a required field");
        }
        if (password.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPassword;
            editTextPassword.setError("Password is a required field");
        }
        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            progressDialog.setMessage("Sign in...");
            progressDialog.show();
            progressDialog.setCancelable(false);
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, "Login Successfull.", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

}
