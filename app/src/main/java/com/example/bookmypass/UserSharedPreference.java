package com.example.bookmypass;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSharedPreference {


    private final String FILENAME = "MyCafeteria";
    private final String KEY_ROLE = "role";
    private final String KEY_ADDRESS = "address";
    private final String KEY_USERNAME = "username";
    private final String KEY_MOBILE = "mobile";
    private final String KEY_TOKEN = "token";
    private final String KEY_WALLET = "wallet";

    // VARIABLES
    private SharedPreferences sharedPreferences;

    public UserSharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
    }

    private void putValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setRole(String role) {
        putValue(KEY_ROLE, role);
    }

    public String getRole() {
        return sharedPreferences.getString(KEY_ROLE, null);
    }


    public void setAddress(String address) {
        putValue(KEY_ADDRESS, address);
    }

    public String getAddress() {
        return sharedPreferences.getString(KEY_ADDRESS, null);
    }

    public void setMobile(String mobile) {
        putValue(KEY_MOBILE, mobile);
    }

    public String getMobile() {
        return sharedPreferences.getString(KEY_MOBILE, null);
    }

    public void setToken (String token)
    {
        putValue(KEY_TOKEN, token);

    }
    public String getToken()
    {
        return sharedPreferences.getString(KEY_TOKEN,null);
    }


    public void setUsername (String username)
    {
        putValue(KEY_USERNAME, username);

    }
    public String getUsername()
    {
        return sharedPreferences.getString(KEY_USERNAME,null);
    }

    public void setWallet (int token)
    {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_WALLET, token);
        editor.commit();

    }
    public int getWallet()
    {
        return sharedPreferences.getInt(KEY_WALLET,0);
    }

    public void clearSharedPreferences() {
        setAddress(null);
        setRole(null);
        setMobile(null);
        setToken(null);
        setWallet(0);
    }
}
