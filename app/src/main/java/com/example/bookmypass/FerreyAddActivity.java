package com.example.bookmypass;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.example.bookmypass.models.PassModel;
import com.example.bookmypass.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class FerreyAddActivity extends AppCompatActivity {

    AppCompatSpinner Source, Destination, Time;
    EditText passengerCount;
    Button DatePicker, Submit;
    SimpleDateFormat df,df1;
    TextView tvFare;
    String formattedDate,currentDate,formattedDate1;
    String[] harbour = {"Gateway", "Rewas", "Elephanta", "Mandva", "Bhaucha Dhakka"};
    String[] option1 = {"Rewas", "Elephanta", "Mandva"};
    String[] option2 = {"Gateway", "Bhaucha Dhakka"};
    String[] Timings={"9:00 am","10:00 am","11:00 am","12:00 pm","1:00 pm","2:00 pm","3:00 pm","4:00 pm","5:00 pm"};
    ArrayAdapter<String> adp;
    String src="",dest="";
    int fare=0;

    int wallet =0;
    String passId;
    int count;

    //Firebase
    FirebaseFirestore mFirestore;
    FirebaseAuth mAuth;
    String userId;
    UserSharedPreference userSharedPreference;


    private int mYear, mMonth, mDay, mHour, mMinute;
    Date selecteddate;
    Calendar c1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ferrey_add);
        userSharedPreference = new UserSharedPreference(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        userId = mAuth.getUid();
        passId = randomNo();

        tvFare = findViewById(R.id.tvFare);
        Source =  findViewById(R.id.source);
        Destination =  findViewById(R.id.destination);
        Time =  findViewById(R.id.time);
        DatePicker = (Button) findViewById(R.id.date);
        passengerCount = (EditText) findViewById(R.id.passenger);
        Submit = (Button) findViewById(R.id.submit);


        df = new SimpleDateFormat("dd/MM/yyyy");
        c1 = Calendar.getInstance();
        selecteddate = c1.getTime();
        currentDate= df.format(c1.getTime());
        formattedDate = currentDate;

        adp = new ArrayAdapter<>(FerreyAddActivity.this, android.R.layout.simple_list_item_1, harbour);
        Source.setAdapter(adp);
        adp = new ArrayAdapter<>(FerreyAddActivity.this, android.R.layout.simple_list_item_1, Timings);
        Time.setAdapter(adp);

        SimpleDateFormat dff = new SimpleDateFormat("dd/MM/yyyy");
        String date = dff.format(Calendar.getInstance().getTime());

        DatePicker.setText(date);
        Source.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                src=Source.getSelectedItem().toString();
                if (Source.getSelectedItem().toString().matches("Gateway")||Source.getSelectedItem().toString().matches("Bhaucha Dhakka")) {
                    adp = new ArrayAdapter<>(FerreyAddActivity.this, android.R.layout.simple_list_item_1, option1);
                    Destination.setAdapter(adp);
                } else {
                    adp = new ArrayAdapter<>(FerreyAddActivity.this, android.R.layout.simple_list_item_1, option2);
                    Destination.setAdapter(adp);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dest=Destination.getSelectedItem().toString();
                if(src.matches("Gateway")){
                    if(dest.matches("Mandwa")){
                        fare=150;
                    }else if(dest.matches("Rewas")){
                        fare=130;
                    }else if(dest.matches("Elephanta")){
                        fare=90;
                    }
                }
                else if(src.matches("Bhaucha Dhakka")){
                    if(dest.matches("Mandwa")){
                        fare=130;
                    }else if(dest.matches("Rewas")){
                        fare=110;
                    }else if(dest.matches("Elephanta")){
                        fare=70;
                    }
                }else if(src.matches("Mandwa")){
                    if(dest.matches("Bhaucha Dhakka")){
                        fare=130;
                    }else if(dest.matches("Gateway")){
                        fare=150;
                    }
                }else if(src.matches("Rewas")){
                    if(dest.matches("Bhaucha Dhakka")){
                        fare=110;
                    }else if(dest.matches("Gateway")){
                        fare=130;
                    }
                }else if(src.matches("Elephanta")){
                    if(dest.matches("Bhaucha Dhakka")){
                        fare=70;
                    }else if(dest.matches("Gateway")){
                        fare=90;
                    }
                }
               // Toast.makeText(FerreyAddActivity.this, "Fare"+fare, Toast.LENGTH_SHORT).show();
               // tvFare.setText("Rs. "+fare+"");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        DatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(FerreyAddActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(android.widget.DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                Calendar td = Calendar.getInstance();
                                c.add(Calendar.DATE,1);
                                Date today = c.getTime();

                                c1.set(Calendar.YEAR, year);
                                c1.set(Calendar.MONTH, monthOfYear);
                                c1.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                                DatePicker.setText(currentDate);
                                if(selecteddate.before(today)){
                                    Toast.makeText(FerreyAddActivity.this, "Bookings only possible for Tomorrow and After", Toast.LENGTH_SHORT).show();
                                    df = new SimpleDateFormat("dd/MM/yyyy");


                                    formattedDate= df.format(today.getTime());
                                    DatePicker.setText(formattedDate);
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        passengerCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0) {
                    tvFare.setText("Rs. "+fare * Integer.parseInt(s.toString()) + "");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                boolean shouldCancelSignUp = false;
                View focusView = null;


                if (passengerCount.getText().toString().equals("")) {
                    shouldCancelSignUp = true;
                    focusView = passengerCount;
                    passengerCount.setError("Source is a required field");
                }else {
                    count =Integer.parseInt(passengerCount.getText().toString());
                }
                if(count>10||count<0){
                    Toast.makeText(FerreyAddActivity.this, "Invalid No. Of Passengers\n(Should be between 1-10)", Toast.LENGTH_SHORT).show();
                }
                if(formattedDate.matches(currentDate)){
                    Toast.makeText(FerreyAddActivity.this, "Please select the future Date", Toast.LENGTH_SHORT).show();
                }
                else if (shouldCancelSignUp) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }
                else{
                    String x="Source : "+src+
                            "\nDestination : "+dest+
                            "\nPassengers Count : "+passengerCount.getText().toString()+
                            "\nDate : "+DatePicker.getText().toString()+
                            "\nTime : "+Time.getSelectedItem().toString()+
                            "\nFare : "+(fare*count);

                    Toast.makeText(FerreyAddActivity.this, ""+x, Toast.LENGTH_SHORT).show();

                    if (fare*count > userSharedPreference.getWallet())
                    {

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(FerreyAddActivity.this, SweetAlertDialog.WARNING_TYPE);
                        sweetAlertDialog .setTitleText("Please Recharge your Wallet...!!!")
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        startActivity(new Intent(FerreyAddActivity.this,MyWalletActivity.class));
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();

                    }else {

                        Toast.makeText(FerreyAddActivity.this, "Success", Toast.LENGTH_SHORT).show();


                        PassModel passModel = new PassModel(userId, passId, src, dest, "Ticket",formattedDate, formattedDate,count+"",new SimpleDateFormat("H:mm a", Locale.getDefault()).format(new Date()), fare*count,"FERRY","",DateFormatHelper.getISOString(new Date()));


                        mFirestore.collection(PassModel.PASS_COLLECTION)
                                .add(passModel)
                                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentReference> task) {
                                        if (task.isSuccessful()) {

                                            wallet = userSharedPreference.getWallet() - fare*count;
                                            userSharedPreference.setWallet(wallet);

                                            Map<String, Object> map = new HashMap<>();
                                            map.put("wallet", wallet);


                                            mFirestore.collection(UserModel.USERS_COLLECTION)
                                                    .document(userId)
                                                    .update(map)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {

                                                        }
                                                    });

                                            Toast.makeText(FerreyAddActivity.this, "Ticket Added", Toast.LENGTH_SHORT).show();

                                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(FerreyAddActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                                            sweetAlertDialog.setTitleText("Ticket Booked Successfully")
                                                    .setConfirmText("Ok")
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                                                            sweetAlertDialog.dismiss();
                                                            Utility.addNotification(FerreyAddActivity.this,"Ferry Ticket Booked",src+" To "+dest);
                                                            Gson gson = new Gson();
                                                            String data = gson.toJson(passModel);

                                                            Intent intent =new Intent(FerreyAddActivity.this,PassDetailActivity.class);
                                                            intent.putExtra("DATA",data);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    })
                                                    .show();
                                        } else {
                                            Toast.makeText(FerreyAddActivity.this, (CharSequence) task.getException(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(FerreyAddActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                }
            }
        });


    }

    public String randomNo() {
        char[] chars1 = "0123456789".toCharArray();
        StringBuilder sb1 = new StringBuilder("F-");
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return sb1.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
