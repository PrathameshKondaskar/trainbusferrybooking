package com.example.bookmypass;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bookmypass.models.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PaymentActivity extends AppCompatActivity {

    EditText editTextCardNo, editTextMobile, editTextCVV, editTextExpiry, editTextPostal,editTextAmount;
    Button buttonSubmit;

    UserSharedPreference userSharedPreference;
    FirebaseFirestore mFirestore;
    String userId;
    AlertDialog dialog = null;
    int wallet ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        mFirestore = FirebaseFirestore.getInstance();
        userId = FirebaseAuth.getInstance().getUid();
        userSharedPreference = new UserSharedPreference(this);

        wallet = userSharedPreference.getWallet();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editTextCardNo = (EditText) findViewById(R.id.editTextCardNo);
        editTextMobile = (EditText) findViewById(R.id.editTextMobile);
        editTextCVV = (EditText) findViewById(R.id.editTextCVV);
        editTextExpiry = (EditText) findViewById(R.id.editTextExpiry);
        editTextPostal = (EditText) findViewById(R.id.editTextPostal);
        editTextAmount = (EditText) findViewById(R.id.editTextAmount);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);



        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toPay();
            }
        });

    }

    public void toPay() {
        String card = editTextCardNo.getText().toString();
        String mobile = editTextMobile.getText().toString();
        String cvv = editTextCVV.getText().toString();
        String expiry = editTextExpiry.getText().toString();
        String postal = editTextPostal.getText().toString();
        String amount = editTextAmount.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (card.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextCardNo;
            editTextCardNo.setError("Card no. is a required field");
        }
        if (cvv.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextCVV;
            editTextCVV.setError("CVV is a required field");

        }
        if (expiry.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextExpiry;
            editTextExpiry.setError("Expiry date is a required field");

        }
        if (postal.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPostal;
            editTextPostal.setError("Postal Code date is a required field");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a required field");
        }
        if (amount.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextAmount;
            editTextAmount.setError("please enter the amount");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. must be of 10 digit");
        }
        if (card.length() > 16 || card.length() < 16) {
            shouldCancelSignUp = true;
            focusView = editTextCardNo;
            editTextCardNo.setError("Card no. must be 16 digit");
        }
        if (cvv.length() > 3 || cvv.length() < 3) {
            shouldCancelSignUp = true;
            focusView = editTextCVV;
            editTextCVV.setError("CVV must be 3 digit");
        }
        if (postal.length() < 6 || postal.length() > 6) {
            shouldCancelSignUp = true;
            focusView = editTextPostal;
            editTextPostal.setError("Postal Code must be 6 digit");
        }


        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            wallet = wallet + Integer.parseInt(amount);
            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
            Map<String, Object> map = new HashMap<>();
            map.put("wallet", wallet);


            mFirestore.collection(UserModel.USERS_COLLECTION)
                    .document(userId)
                    .update(map)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(PaymentActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialog .setTitleText("Amount addd Successfully")
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                                            userSharedPreference.setWallet(wallet);
                                           // startActivity(new Intent(PaymentActivity.this,MyWalletActivity.class));
                                            finish();
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();

                        }
                    });


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
