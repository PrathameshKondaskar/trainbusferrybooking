package com.example.bookmypass.models;

public class TrainModel {

    public static final String TRAIN_COLLECTION = "Trains";

    String id ;
    String routes;
    String time;
    String source;
    String Ddestination;

    public TrainModel(String id, String routes, String time, String source, String ddestination) {
        this.id = id;
        this.routes = routes;
        this.time = time;
        this.source = source;
        Ddestination = ddestination;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoutes() {
        return routes;
    }

    public void setRoutes(String routes) {
        this.routes = routes;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDdestination() {
        return Ddestination;
    }

    public void setDdestination(String ddestination) {
        Ddestination = ddestination;
    }
}
