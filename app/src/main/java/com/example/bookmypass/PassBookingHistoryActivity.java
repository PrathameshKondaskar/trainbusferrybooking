package com.example.bookmypass;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.bookmypass.models.PassModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PassBookingHistoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProgressBar progressBar;

    //Firebase
    FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String userId;
    String date;

    ArrayList<PassModel> passModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_booking_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView= findViewById(R.id.recycleViewHistoty);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        userId = user.getUid();
        mFirestore = FirebaseFirestore.getInstance();

        getDataFromFirebase();


    }

    public void getDataFromFirebase(){

        mFirestore.collection(PassModel.PASS_COLLECTION)
                .orderBy("timeStamp", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful())
                        {
                            progressBar.setVisibility(View.GONE);
                            passModelArrayList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                if(userId.matches(document.getData().get("userId").toString()))
                                {
                                    PassModel passModel = document.toObject(PassModel.class);
                                    passModelArrayList.add(passModel);
                                }
                            }
                            CustomAdapter customAdapter = new CustomAdapter(PassBookingHistoryActivity.this,passModelArrayList);
                            recyclerView.setAdapter(customAdapter);
                        }
                        else
                        {

                        }
                    }
                });

    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.RecyclerHolder> {


        Context context;
        List<PassModel> passModelList;


        public CustomAdapter(Context context, List<PassModel> models) {
            this.context = context;
            this.passModelList = models;
        }

        @NonNull
        @Override
        public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.custom_history, parent, false);


            return new RecyclerHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {

            date = passModelList.get(position).getFromDate();

            Calendar c = Calendar.getInstance();
            SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
            Date dt1= null;
            try {
                dt1 = format1.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.setTime(dt1);

            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            String dayof_month = outFormat.format(dt1);

            SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
            String month = monthFormat.format(dt1);

            SimpleDateFormat YearFormat = new SimpleDateFormat("yyyy");
            String year = YearFormat.format(dt1);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
            String currentDate = dateFormat.format(dt1);

            String logo = passModelList.get(position).getPassName();

            if(logo.matches("BUS")){
               holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_bus_red_24dp));
            }
            if(logo.matches("TRAIN")){
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_train_red_24dp));
            }
            if(logo.matches("FERRY")){
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_boat_red_24dp));
            }



            holder.tvFare.setText("Rs."+passModelList.get(position).getTotalFare()+"");
            holder.tvSource.setText(passModelList.get(position).getSource());
            holder.tvDestination.setText(passModelList.get(position).getDestination());
            holder.tvType.setText(passModelList.get(position).getType());
            holder.tvPassId.setText(passModelList.get(position).getPassId());

            holder.tvFromdate.setText(currentDate+""+getDayNumberSuffix(Integer.parseInt(currentDate)));
            holder.tvMonthDay.setText(month+","+dayof_month);
            holder.tvYear.setText(year);



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Gson gson = new Gson();
                    String data = gson.toJson(passModelList.get(position));

                    Intent intent =new Intent(PassBookingHistoryActivity.this,PassDetailActivity.class);
                    intent.putExtra("DATA",data);
                    startActivity(intent);



                }
            });




        }

        @Override
        public int getItemCount() {
            return passModelList.size();
        }

        public class RecyclerHolder extends RecyclerView.ViewHolder {

            TextView tvFromdate, tvFare,tvSource,tvDestination,tvType,tvPassId,tvMonthDay,tvYear;
            ImageView imageView;

            public RecyclerHolder(@NonNull View itemView) {
                super(itemView);
                tvFromdate = itemView.findViewById(R.id.tvFromDate);
                tvFare = itemView.findViewById(R.id.tvFare);
                tvSource = itemView.findViewById(R.id.tvSource);
                tvDestination = itemView.findViewById(R.id.tvDestination);
                tvType = itemView.findViewById(R.id.tvtype);
                tvPassId = itemView.findViewById(R.id.tvPassId);
                imageView = itemView.findViewById(R.id.imageView);
                tvMonthDay = itemView.findViewById(R.id.tvMonthDay);
                tvYear = itemView.findViewById(R.id.tvYear);
            }
        }

    }


    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    }
